#include <iostream>
#include <fstream>
#include <vector>

#define RED { 255, 0, 0 }
#define GREEN { 0, 255, 0 }
#define BLUE { 0, 0, 255 }
#define WHITE { 255, 255, 255 }
#define BLACK { 0, 0, 0 }
#define YELLOW { 255, 255, 0 }

#define RED1 { 125, 0, 0 }
#define RED2 { 150, 0, 0 }
#define RED3 { 175, 0, 0 }
#define RED4 { 200, 0, 0 }
#define RED5 { 255, 0, 0 }
#define RED6 { 255, 25, 25 }
#define RED7 { 255, 50, 50 }
#define RED8 { 255, 75, 75 }
#define RED9 { 255, 100, 100 }
#define RED10 { 255, 150, 150 }

using namespace std;

struct place {
	int x;
	int y;
};

struct cell {
	place position;
	bool stem;
	unsigned char health;
};

struct color {
	int r;
	int g;
	int b;
};

void initImage(vector<vector<color>> &image, int w, int h);
int writeImage(vector<vector<color>> &image, string filename);
vector<vector<color>> resizeMatrix(vector<vector<color>> plocha,int size);
void printMatrix(vector<vector<unsigned char>> plocha, int &image_index);
void printCells(vector<cell> cells, int &image_index, int matrix_size);
