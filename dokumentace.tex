\documentclass[10pt]{article}
\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{pgfplots}

\pgfplotsset{compat=1.12}

\begin{document}

\begin{titlepage}
	\centering
	{\scshape\LARGE Vysoké Učení Technické v Brně \par}
	{\scshape\Large Fakulta Informačních Technologií \par}
	\vspace{2cm}
	{\scshape\Large Dokumentace k projektu z předmětu IMS\par}
	\vspace{2cm}
	{\huge\bfseries Aplikace celulárních automatů v biologii\par}
	\vspace{2cm}
	\vfill
	{\Large Michal Moravec\par}
	{\Large Karel Červíček\par}
	\vspace{2cm}
	{\large \today\par}
\end{titlepage}

\tableofcontents
\newpage

\section{Úvod}
Celá práce se zabývá návrhem a implementací celulárního automatu v oblasti medicíny a to konkrétně modelováním chování nádorových buněk. Model ukazuje s abstrakcí jistých vlastností, které jsou popsány dále, růst nádorů z jedné buňky kontinuálně po dobu určenou experimentem. Je předpokládáno vytvoření modelu celulárního automatu růstu nádoru s oblastí, která se dynamicky zvětšuje s růstem populace nádorových buněk. V této práci se budeme snažit rekapitulovat a re-implementovat více stupňovou Monte Carlo simulaci růstu nádoru, následně vše zhodnotíme novými experimenty. Hodnotit bude výkon a chování nádorového růstu.

Důležitým aspektem celulárních automatů je kvalitní návrh implementace, která by měla být co nejvíce optimalizována. Celulární automaty jsou čím dál častěji používaným nástrojem pro zkoumání nádorového bujení. Stále je v dnešní době nepřestavitelné vytvořit komplexní simulaci stochastického pohybu nádorových buněk a sledování celkové dynamiky nádorové populace a to jak z hlediska nedostatku výpočetního výkonu, tak z hlediska nedostatku informací, tento problém je vysvětlen v sekci míry abstrakce \ref{abstrakce}.
 
Jedná se o práci nad velkým počtem dat s ohledem na nemalý vektor parametrů. Výpočetní výkon je zásadní pro objektivní výsledky experimentů v oblasti modelování principů chování buněk, kdy je třeba, aby modelový čas odrážel reálný časový průběh měsíce až roky s ohledem na fakt, že aktualizace stavu je v rámci hodin až minut. Čili se jedná o souvislý průběh vývoje od počáteční buňky až po nádorovou populaci v řádech statisíc buněk, tyto buňky souvisle ovlivňují své okolí netriviálními interakcemi. Celkovým návrhem optimalizace, logikou chování a datových struktur je podrobně popsána v sekci způsob řešení \ref{reseni}.

Základním přínosem vytvoření modelu celulárního automatu, který modeluje růst nádoru je možnost sledovat chování každé buňky a sledování prav\-dě\-po\-dob\-né\-ho růstu bez počáteční znalosti uskupení nádoru.

\subsection{Zdroje faktů}
Základním zdrojem faktů je odborný článek \cite{article1}, v této práci budou využity matematické vztahy a konstanty z této publikace, neboť naším cílem je tento koncept vytvořit znovu a následně zpracovat vlastní experimenty s nově vytvořeným modelem.

\subsection{Ověření validity}
Na všechny hodnoty v článku jsme udělali vlastní rozbor a také jsme je křížově ověřili \cite{article2}. Celkový koncept a především míru abstrakce od reálného systému jsme konzultovali s MUDr. Karlem Červíčkem. Ověřili jsme validní chování na malém počtu buněk, u kterých jsme sledovali jejich chování, kdy jsme porovnávali jejich historickou polohu s polohou aktuální. Podrobný popis testování v kapitole testování \ref{testovani}. 

\section{Fakta}
Každá nádorová buňka zabírá prostor $10 \mu m^2$ \cite{article1}. Jedná se o samostatnou entitu v rámci dvourozměrné matice, která představuje prostor nádorového bujení. Každá buňka je charakterizována vektorem vlastností. V našem modelu bude tento vektor obsahovat čtyři hodnoty [$cct, \rho, \mu, \alpha$], kde $cct$ je cell cycle time, $\rho$ představuje potenciál proliferace, $\mu$ značí potenciál migrace a jako poslední parametr $\alpha$ je pravděpodobnost spontánní smrti \cite{article1}. 

Čas reálného systému je diskretizován. Časový rozdíl mezi dvěma aktualizacemi $\Delta t$ je $1/24$ \cite{article1}, což odráží jednu hodinu v reálném systému. Celý model je stochastický, každá akce jednotlivé buňky je řízena prav\-dě\-po\-dob\-nost\-í.

Proliferace a migrace buněk probíhají jako náhodné jevy s určitou prav\-dě\-po\-dob\-nost\-í. Když u buňky neproběhne spontánní smrt, tak buď proliferuje s prav\-dě\-po\-dob\-nost\-í $p_d=(24 hodin/cct) \times \Delta t$ \cite{article1}, nebo migruje s pravděpodobností $p_m=\mu \times \Delta t$ \cite{article1}. Dále u kmenových buněk, které jsou popsány dále, probíhá asymetrická proliferace, která je daná proměnnou $p_s$.

Náš konkrétní model je inicializován s parametry [$cct=24 hodin$, $\rho =10$, $\mu =100 \mu m /den$, $\alpha =1\%$] a $p_s =10\%$ \cite{article1}. Parametr $\mu$ je třeba převést na hodnotu příslušnou našemu modelu, kde velikost jedné buňky je $10 \mu m^2$, takže výsledná rychlost je 10 políček matice za den. Z toho vyplývají následující hodnoty pro inicializaci simulace: 
$$p_d=(24/24) \times (1/24)=1/24$$
$$p_m=10 \times (1/24) = 10/24$$

\section{Koncepce}
Model představuje heterogenní tumor. Populace takového tumoru obsahuje buň\-ky kmenové a buňky nekmenové. U buněk kmenových je proliferační potenciál nekonečný, neboť tyto buňky z pohledu našeho modelu neumírají. Druhý typ buněk má omezenou životnost, která se snižuje s každou novou proliferací dané buňky. Čili životnost buňky se nesnižuje při migraci ani při stavu, kdy nic nedělá. Dělení má zásadní vlastnosti, kdy oba typy buněk mohou provést symetrické dělení, kdy daná buňka vytvoří dvě dcery. Ty zdědí vlastnosti buňky rodičovské. Tato funkcionalita je zásadní pro validní model, a je tedy opravdu důležité, aby nové dvě buňky měly hodnoty parametrů jako má buňka rodičovská. 

Oba typy buněk provádí symetrickou proliferaci, kdy jak bylo řečeno výše, jsou zděděny všechny vlastnosti. Buňky kmenové navíc provádí dělení asymetrické, kdy s pravděpodobností $p_s$ vytvoří další kmenovou buňku a s prav\-dě\-po\-dob\-nost\-í $1-p_s$ vytvoří buňku nekmenovou, která má maximální proliferační potenciál. Každá nová buňka je schopna proliferovat pouze na volné místo v daném okolí, stejně tak je tímto podmíněna migrace buňky. Pokud je okolí dané buňky v dvourozměrné matici zaplněno jinými buňkami, stane se neaktivní. Každá nekmenová buňka také podléhá spontánní smrti, pokud se tato vlastnost u dané buňky projeví, je buňka z modelu odstraněna.

\subsection{Míra abstrakce modelu}
\label{abstrakce}
Charakteristická vlastnost nádorových buněk je rezistence proti apoptóze. Vyznačují se nekontrolovatelným buněčným dělením a jistému znemožnění jejich opotřebovanosti. Poškozená buňka má překračující dělení a ztrácí závislost na interakci s přítomnými různými hormony a růstovými faktory, a dalšími fyzikálně-chemickými vlivy, které přicházejí z vnějšku.

Autokrinní stimulace je také součástí nádorového bujení, ve zkratce se jedná o akci, kdy daná buňka vylučuje látky, které ji dále ovlivňují, jedná se o specifické růstové faktory.  Postižena je především regulace přechodu z G1-fáze do S-fáze. Díky tomu, že buňka má tendenci vymykat se fyziologickému mechanismu dělení a jeho kontrolním mechanizmům, hraje zde roli velké množství vlivů. Hlavní je aberantní regulace cyklu buněčného dělení.

Může postihnout buňky všech tkání. Mutace v protoonkogenech, tumor-supresorových genech, mutátorových genech vedou k transformaci buňky v buňku nádorovou. Maligní transformace je hlavním rysem a důsledkem zejména poruch v přirozených regulačních mechanizmech růstu a diferenciace buněk.  
Nicméně i přes značnou autonomii růstu maligních buněk jsou i tyto procesy in vivo ovlivnitelné a ovlivňované zjevnými faktory. Stejné faktory se někdy projevují různě dle typu nádoru, na druhou stranu tytéž faktory u téhož nádoru mohou mít různý vliv u různých jedinců díky působení komplexů jiných faktorů, dochází zde tedy k značné individuální variabilitě.  Těchto faktorů je těžko postižitelné množství, nicméně v globále je možné v rámci matematických modelů použít abstrakce tak, aby se model co nejvíce blížil realitě.

Tímto se chceme dostat k tomu, že není možné v tomto projektu prověřovat všechny vlastnosti a provedli jsme vlastní ověření abstrakce, která je v referenčním článku, také je celý tento odstavec  konzultován s osobou, která danému tématu rozumí (MUDr. Karel Červíček).

\section{Způsob řešení}
\label{reseni}
\subsection{Datové struktury}
Z referenčního článku jsme se řídili radami, jak celkový model optimalizovat. Tyto kroky jsme diskutovali a následně implementovali v našem modelu.  Dů\-le\-ži\-tým bodem je volba vhodných datových struktur. Bude třeba pracovat s dvoj\-roz\-měr\-nou maticí. Celý program je psán v jazyce C++, kde je vhodné využít knihovnu datových kontejnerů STL. Tato knihovna poskytuje ukládání vlastních datových struktur a umožňuje efektivní práci nad těmito daty. Jsou zde iterátory a funkce, které jsou optimalizovány. Také jsme se snažili o paměťovou efektivitu. Dvourozměrná matice, uchovávající informaci o stavu míst v prostoru, zda je volné nebo ne, je definována jako \texttt{unsigned char}. Dále je vytvořeno jednorozměrné pole, ve kterém jsou ukládány jednotlivé buňky. Toto je zásadní, neboť nebude zapotřebí procházet celou dvourozměrnou matici volných míst. Buňka je reprezentována strukturou \texttt{cell} obsahující:
\begin{enumerate}
\item \texttt{place position}  - struktura obsahující pozici v dvourozměrné matici u\-cho\-vá\-va\-jí\-cí informaci o stavu míst
\item \texttt{bool stem} – zda se jedná o buňku kmenovou
\item \texttt{unsigned char health} – pro marametr modelu p, indikující životnost buňky
\end{enumerate}

\subsection{Logika}
Celkový princip je přímočarý. V této sekci bude hrubě vysvětlen celkový princip programu, ale také se zde zaměříme za důležité principy, díky kterým je program relativně rychlý. Celý program je postavený na třech funkcích:
\begin{enumerate}
\item \texttt{initMatrix} – inicializace matice stavu míst
\item \texttt{initCells} vytvoření prvních buňek
\item \texttt{void simulate} – hlavní běh simulace, zde je celé rozhodování v závislosti na parametrech definovaných v sekci koncepce
\end{enumerate}

Jak jsme uvedli, prochází se pouze pole buněk \texttt{cells}. Toto pole je nejdříve náhodně zamícháno, aby daný krok byl náhodný a výsledky experimentů nebyly ovlivněny pořadím buněk v poli. Je třeba si uvědomit, že každá akce jedné buňky ovlivní okolí a tím ovlivní i interakci jiných buněk s tímto okolím. Díky tomu je třeba při každé aktualizaci matice nový stav jednotlivých buněk ukládat do pomocného pole buněk a tímto polem nových dat přepsat pole \texttt{cells}.

\subsection{Řešení náročné operace výběru prázdných míst}
Tento bod je zásadní na rychlost programu. V matici je uloženo číslo 0-9, které indikuje:
\begin{itemize}
\item 9 – označuje, že je místo volné 
\item 0-8 – označuje, kolik volných míst je v okolí dané buňky a zároveň to, že se buňka na daném místě nachází
\end{itemize}

S tímto přístupem není nutné matici procházet při hledání volného prostoru. Díky uložené pozici ve struktuře \texttt{cell} je možné přistoupit přímo do matice. V matici je uloženo číslo udávající počet volných míst v okolí, díky kterému je pak konkrétní místo vybráno tak, že se zamíchá vektor, který obsahuje všechny pozice okolí a pak se do něj v cyklu přistupuje. Nakonec je tedy přistoupeno k volné pozici.

\subsection{Poznámka k podpoře stochastického modelu}
Pravděpodobnost jsme se rozhodli generovat pomoci generátoru náhodných čísel Mersenne twister, vzhledem k doporučení Dr. Ing. Petra Peringera. Vzhledem k vlastnostem tohoto generátoru se jedná o spolehlivou podporu pro generování pseudonáhody s rovnoměrným rozložením.

\subsection{Specifikace spouštění a vlastností výstupu}
Program je možné přeložit příkazem make a následně spustit pomocí make run. Důležité upozornění: program bude spuštěn po dobu 180 dní modelového času, pro představu reálného běhu je čtenáři doporučeno pročíst kapitolu experimentování, kde je proveden experiment s ohledem na výpočetní náročnost. Pokud bude třeba simulaci spustit s jiným parametrem dní, je to možné takto:
\texttt{./ca <dny>}, kde parametr dny, za který je možné dosadit celé číslo, značí počet dní modelového času. Při spuštění ovšem mějte na paměti náročnost výpočtu.

Další důležitou vlastností programu je grafický výstup. Pro grafickou zpětnou vazbu programu je pro každý simulovaný den vygenerován obrázek odrážející stav matice a rozložení buněk. Barevný výstup má následující sémantiku:
\begin{itemize}
\item Žlutá barva indikuje kmenové buňky
\item Bílá prázdné místo 
\item Odstín červené značí životnost nekmenové buňky, kde nejsvětlejší odstín je buňka s maximální životností a nejtmavší odstín je s minimální
\end{itemize}

\section{Testování}
\label{testovani}
V této práci jsme dali velký důraz na validaci a testování implementovaných algoritmů. V úvodu bylo řečeno, že hodnoty a míra abstrakce byla konzultována a článek byl celkově ověřován i jinými zdroji \cite{article2}.
Zde bude rozvedeno, jak byl program testován a jak bylo ověřováno jeho správné chování. Byl implementován základ hlavního algoritmu pro ověření, že se aktualizace matice a pohyb buněk koná validně. Byl vytvořen speciální výstup upravený tak, aby vhodně zobrazoval buňky v malé matici a barevně rozlišoval tři stavy:
\begin{enumerate}
\item \textbf{zelená} – touto barvou je označena buňka, která se zrodí
\item \textbf{modrá} – buňka, která migrovala, čili je možná změna zelené na modrou 
\item \textbf{černá} – historické místo, ze kterého buňka migrovala 
\end{enumerate}

Bylo testováno několik pravidel, které je nutné dodržet. Například černé místo by mělo být vedle místa zeleného a rozhodně by nemělo docházet k svévolnému cestování mimo populaci o více míst. Také je třeba dodržet pravidla pro vzájemnou interakci dvou buněk pro společné okolí, není možná například proliferace na místo, kde v dané aktualizaci již jiná buňka proliferovala.

Takovýchto testů bylo provedeno 24. Pro vizuální představu zde uvedeme dva obrázky na jeden test (celkem tedy 4 obrázky). Při testování byl generován obraz matice po každé aktualizaci, aby bylo možné kontrolovat každý krok.

V druhé aktualizaci, která je vidět na obrázku \ref{fig:aktualizace2}, jsou buňky, které proliferovaly a modrá buňka migrující zprava doleva. Následně tento stav můžeme porovnat s aktualizací číslo tři na obrázku \ref{fig:aktualizace3}.  Můžeme vidět dvě validní proliferace a jednu migraci, kdy zelená buňka nejvíce vlevo migrovala směrem nahoru, modrá a dolní zelená konaly proliferaci.

Jak je vidět, pohyb buněk je validní. V horní části obrázku \ref{fig:aktualizace4} všechny buňky migrovaly. V dolní části obrázku \ref{fig:aktualizace4} zelená zrozená buňka proliferovala do horního pravého rohu obrázku \ref{fig:aktualizace5}.
Takto podobně byly testovány jednotlivé části programu, pro spolehlivost celého programu a také v rámci celého vývoje bylo vše kontrolováno.  

\section{Experimenty a poznatky}
Celkem budou provedeny dva experimenty. Budeme se zaměřovat na základní cíle, které jsme stanovili v úvodu. To je sledování výpočetní náročnosti a evoluce nádorového bujení. Růst nádoru by měl podléhat jistým vlastnostem, které budou zkoumány. 

Důležité bude sledovat strukturu invazivních částí, což jsou výběžky v periferii, a hlavním bodem je tvorba samotného tumoru ve vnitřní části, který je tvořený z buněk kmenových.

První experiment bude spuštěn pro simulaci 180 dní růstu s parametry uvedenými v referenčním článku. Výsledky experimentu můžeme pozorovat na obrázcích \ref{fig:e1d45}, \ref{fig:e1d85}, \ref{fig:e1d115}, \ref{fig:e1d145} a \ref{fig:e1d180}. Je také přiložena tabulka \ref{tab:e1} s dodatečnými statistikami a hodnoty z této tabulky jsou vykresleny v grafu na obrázku \ref{fig:e1g}.

Druhý experiment je spuštěn se stejnými parametry jako první experiment, pozorovat budeme změny vůči prvnímu experimentu, jelikož se buňky šíří podle stochastických pravidel. K experimentu jsou opět přiloženy obrázky \ref{fig:e2d45}, \ref{fig:e2d85}, \ref{fig:e2d115}, \ref{fig:e2d145} a \ref{fig:e2d180}, graf \ref{fig:e2g} a tabulka s hodnotami grafu \ref{tab:e2}.

\subsection{Porovnání}
Cílem bylo pozorovat evoluci nádorového bujení. Oba experimenty byli spuštěny se stejnými parametry, neboť chceme sledovat stochastické chování simulace v závislosti na těchto parametrech. Můžeme vidět jak invazivní charakter, tak tvorbu nádoru v centru.

\section{Závěr}
Model celulárního automatu v oblasti medicíny je bez pochyb silný nástroj. V této práci byly diskutovány a rekapitulovány pilíře modelování nádorového bujení s pomocí celulárního automatu. Model, který jsme vytvořili, je základem pro další výzkum a experimentování. Silně jsme využívali prostředků jazyka C/C++, který je pro tento účel více než vyhovující. Model byl efektivně testován.

Následné experimenty by měly čtenáři přinést pohled na průběh nádorového bujení, také by si měl odnést jisté teoretické poznatky o tom, jaká je evoluce nádorové buňky v průběhu růstu. Následně průběh celého růstu nádoru. Dále by experimenty měly odpovědět na otázku výpočetní náročnosti v oblasti celulárních automatů. Celkově by měl mít článek edukativní přínos.

\clearpage
\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/aktualizace2.png}
  \caption{aktualizace A1}
  \label{fig:aktualizace2}
\end{figure}

\begin{figure} 
  \centering
    \includegraphics[scale=0.6]{obr/aktualizace3.png}
  \caption{aktualizace A2}
  \label{fig:aktualizace3}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.6]{obr/aktualizace4.png}
  \caption{aktualizace B1}
  \label{fig:aktualizace4}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/aktualizace5.png}
  \caption{aktualizace B2}
  \label{fig:aktualizace5}
\end{figure}

\begin{figure}
\centering
\begin{tikzpicture}
	\begin{axis}[
			xlabel=Počet vypočítaných dní,
			ylabel=Čas výpočtu v milisekundách,
            symbolic x coords={15, 45, 85, 115, 145, 180},
            xtick=data
          ]
            \addplot[ybar,fill=red] coordinates {
                (15, 32)
                (45, 301)
                (85, 3573)
                (115, 13688)
                (145, 35166)
                (180, 81121)
            };
	\end{axis}
\end{tikzpicture}
\caption{Experiment 1: Graf}
\label{fig:e1g}
\end{figure}

\begin{table}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Počet vypočítaných dní & Čas výpočtu v milisekundách\\
\hline
15 & 32\\
\hline
45 & 301\\
\hline
85 & 3573\\
\hline
115 & 13688\\
\hline
145 & 35166\\
\hline
180 & 81121\\
\hline
\end{tabular}
\end{center}
\caption{Experiment 1}
\label{tab:e1}
\end{table}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp1/day45.jpg}
  \caption{Experiment 1: Den 45}
  \label{fig:e1d45}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp1/day85.jpg}
  \caption{Experiment 1: Den 85}
  \label{fig:e1d85}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp1/day115.jpg}
  \caption{Experiment 1: Den 115}
  \label{fig:e1d115}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp1/day145.jpg}
  \caption{Experiment 1: Den 145}
  \label{fig:e1d145}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp1/day180.jpg}
  \caption{Experiment 1: Den 180}
  \label{fig:e1d180}
\end{figure}

\begin{figure}
\centering
\begin{tikzpicture}
	\begin{axis}[
			xlabel=Počet vypočítaných dní,
			ylabel=Čas výpočtu v milisekundách,
            symbolic x coords={15, 45, 85, 115, 145, 180},
            xtick=data
          ]
            \addplot[ybar,fill=red] coordinates {
                (15, 54)
                (45, 680)
                (85, 8131)
                (115, 24304)
                (145, 53771)
                (180, 110924)
            };
	\end{axis}
\end{tikzpicture}
\caption{Experiment 2: Graf}
\label{fig:e2g}
\end{figure}

\begin{table}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Počet vypočítaných dní & Čas výpočtu v milisekundách\\
\hline
15 & 54\\
\hline
45 & 680\\
\hline
85 & 8131\\
\hline
115 & 24304\\
\hline
145 & 53771\\
\hline
180 & 110924\\
\hline
\end{tabular}
\end{center}
\caption{Experiment 2}
\label{tab:e2}
\end{table}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp2/day45.jpg}
  \caption{Experiment 2: Den 45}
  \label{fig:e2d45}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp2/day85.jpg}
  \caption{Experiment 2: Den 85}
  \label{fig:e2d85}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp2/day115.jpg}
  \caption{Experiment 2: Den 115}
  \label{fig:e2d115}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp2/day145.jpg}
  \caption{Experiment 2: Den 145}
  \label{fig:e2d145}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.5]{obr/exp2/day180.jpg}
  \caption{Experiment 2: Den 180}
  \label{fig:e2d180}
\end{figure}

\clearpage
\begin{thebibliography}{9}

\bibitem{article1}
  Jan Poleszczuk,
  Heiko Enderling,
  "A High-Performance Cellular Automaton Model of Tumor Growth with Dynamcially Growing Domains",
  \emph{Applied Mathematics},
 2014, 5, 144-152.
  
\bibitem{article2}
  H. Enderling, A. R. A. Anderson, M. A. J. Chaplain, A. Beheshti, L. Hlatky and P. Hahnfeldt,
  "Paradoxical Dependencies of Tumor Dormancy and Progression on Basic Cell Kinetics",
  \emph{Cancer Research},
 Vol. 69, No. 22, 2009 pp. 8814-8821.

\end{thebibliography}
\end{document}