#include <iostream>
#include <vector>
#include <algorithm>
#include "image_output.h"
#include <chrono>
#include <stdio.h>
#include <random>

#define FREE 9 //volne misto
#define DANGER_POS 4 //kdyz je bunka 4 pozice od okraje, je treba zvetsit matici

using namespace std;
using namespace std::chrono;

//inicializuje matici
void initMatrix(vector<vector<unsigned char>> &v, int size) {
	for (int i = 0; i < size; ++i) {
		vector<unsigned char> tmp;
		for (int o = 0; o < size; ++o) {
			tmp.push_back(FREE);
		}
		v.push_back(tmp);
	}
}

//vypise matici na terminalovy vystup
void printMatrixAscii(vector<vector<unsigned char>> &v) {
	for (int i = 0; i < v.size(); ++i) {
		for (int o = 0; o < v[i].size(); ++o) {
			cout << +v[i][o] << (o < v[i].size() - 1 ? " " : "");
		}
		cout << endl;
	}
	cout << "____________________" << endl;
}

//vlozi na matici prvni bunku
void initCells(vector<vector<unsigned char>> &matrix, vector<cell> &cells) {
	int half = matrix.size() / 2;
	cell c = {{ half, half }, true, 10 };
	cells.push_back(c);

	matrix[c.position.y][c.position.x] = 8;
}

//vrati nahodne volne misto z okoli bunky
place pickFreePlace(vector<vector<unsigned char>> &matrix, int x, int y) {
	vector<place> neighbourhood = {{-1,-1}, {-1,0}, {-1,1}, {0,-1}, {0,1}, {1,-1}, {1,0}, {1,1}}; //relativni pozice okoli
	random_shuffle(neighbourhood.begin(), neighbourhood.end());

	for (int i = 0; i < 8; ++i) {
		if (matrix[y + neighbourhood[i].y][x + neighbourhood[i].x] == FREE) {
			return { x + neighbourhood[i].x, y + neighbourhood[i].y };
		}
	}
}

//aktualizuje matici po proliferaci
void updateMatrixAfterProliferation(vector<vector<unsigned char>> &matrix, place dst) {
	vector<place> neighbourhood = {{-1,-1}, {-1,0}, {-1,1}, {0,-1}, {0,1}, {1,-1}, {1,0}, {1,1}}; //relativni pozice okoli

	matrix[dst.y][dst.x] = 8; //nastavi na dst misto pocet volnych pozic 8, dale se toto cislo bude dekrementovat

	//aktualizujeme vysledny pocet mist, ktery je kolem mista dst
	for (int i = 0; i < neighbourhood.size(); ++i) {
		if (matrix[dst.y + neighbourhood[i].y][dst.x + neighbourhood[i].x] < FREE)
			matrix[dst.y][dst.x]--; //dekrementuje pocet volnych pozic okolo mista dst
	}

	//aktualizuje okoli dst
	for (int i = 0; i < neighbourhood.size(); ++i) {
		if (matrix[dst.y + neighbourhood[i].y][dst.x + neighbourhood[i].x] < FREE)
			matrix[dst.y + neighbourhood[i].y][dst.x + neighbourhood[i].x]--; //dekrementuje pocet volnych pozic okolo mista dst
	}
}

//aktualizuje matici po presunuti
void updateMatrixAfterMove(vector<vector<unsigned char>> &matrix, place src, place dst) {
	vector<place> neighbourhood = {{-1,-1}, {-1,0}, {-1,1}, {0,-1}, {0,1}, {1,-1}, {1,0}, {1,1}}; //relativni pozice okoli

	//aktualizuje okoli src
	for (int i = 0; i < neighbourhood.size(); ++i) {
		if (matrix[src.y + neighbourhood[i].y][src.x + neighbourhood[i].x] < FREE)
			matrix[src.y + neighbourhood[i].y][src.x + neighbourhood[i].x]++; //inkrementuje pocet volnych pozic okolo mista src
	}

	matrix[src.y][src.x] = FREE; //puvodni misto se nastavi na volne
	matrix[dst.y][dst.x] = 8; //nastavi na dst misto pocet volnych pozic 8, dale se toto cislo bude dekrementovat

	//aktualizujeme vysledny pocet mist, ktery je kolem mista dst
	for (int i = 0; i < neighbourhood.size(); ++i) {
		if (matrix[dst.y + neighbourhood[i].y][dst.x + neighbourhood[i].x] < FREE)
			matrix[dst.y][dst.x]--; //dekrementuje pocet volnych pozic okolo mista dst
	}

	//aktualizuje okoli dst
	for (int i = 0; i < neighbourhood.size(); ++i) {
		if (matrix[dst.y + neighbourhood[i].y][dst.x + neighbourhood[i].x] < FREE)
			matrix[dst.y + neighbourhood[i].y][dst.x + neighbourhood[i].x]--; //dekrementuje pocet volnych pozic okolo mista dst
	}
}

//aktualizuje matici po smrti bunky
void updateMatrixAfterDeath(vector<vector<unsigned char>> &matrix, place src) {
	vector<place> neighbourhood = {{-1,-1}, {-1,0}, {-1,1}, {0,-1}, {0,1}, {1,-1}, {1,0}, {1,1}}; //relativni pozice okoli

	matrix[src.y][src.x] = FREE; //puvodni misto se nastavi na volne

	//aktualizuje okoli src
	for (int i = 0; i < neighbourhood.size(); ++i) {
		if (matrix[src.y + neighbourhood[i].y][src.x + neighbourhood[i].x] < FREE)
			matrix[src.y + neighbourhood[i].y][src.x + neighbourhood[i].x]++; //inkrementuje pocet volnych pozic okolo mista src
	}
}

//vrati true, kdyz se bunka nachazi v nebezpecne vzdalenosti od kraje matice
bool checkBoundary(int matrix_size, place position) {
	return (position.x < DANGER_POS || position.x > matrix_size - DANGER_POS || position.y < DANGER_POS || position.y > matrix_size - DANGER_POS);
}

//rozsiri matici do vsech stran o size prvku
void expandMatrix(vector<vector<unsigned char>> &matrix, vector<cell> &cells, int size) {
	int newsize = matrix.size() + 2 * size;
	vector<vector<unsigned char>> v;
	v.resize(newsize);

	//naalokuje misto do docasne matice v
	for (int i = 0; i < v.size(); ++i) {
		v[i].resize(newsize, 9);
	}

	//nakopiruje obsah z puvodni matice do docasne na spravna mista (posunuta doprava dolu o size)
	for (int i = 0; i < matrix.size(); ++i) {
		for (int o = 0; o < matrix[i].size(); ++o) {
			v[i + size][o + size] = matrix[i][o];
		}
	}

	//obsah docasne matice v se nahraje do matrix
	matrix = v;

	for (int i = 0; i < cells.size(); ++i) {
		cells[i].position.x += size;
		cells[i].position.y += size;
	}
}

//hlavni funkce, kde probiha simulace
void simulate(vector<vector<unsigned char>> &matrix, vector<cell> &cells, int iterations) {
	int image_index = 1; //promenna pro znaceni dnu simulace ve jmenech souboru

	//inicializace mersenne twisteru, budeme generovat nahodna cisla v intervalu <0, 1> s rovnomernym rozlozenim
	std::random_device rd; 
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0, 1);

	//parametry simulace
	float proliferate = 1.0/24.0;
	float migration = 10.0/24.0;
	float stem_birth = 0.1;
	float spontanious_death = 0.01;

	high_resolution_clock::time_point t1 = high_resolution_clock::now(); //zacatek simulace

	for (int i = 0; i < iterations; ++i) {
		random_shuffle(cells.begin(), cells.end()); //zamichame s bunkami, at je potom muzeme prochazet nahodne

		float probability;
		bool toExpand = false; //nastavi se na true, pokud je treba zvetsovat matici

		vector<cell> tmp; //vektor pro ukladani bunek z aktualni iterace, ktery nahradi puvodni vektor cells

		//prochazeni vsech bunek
		for (int o = 0; o < cells.size(); ++o) {
			probability = dis(gen);

			//dochazi ke spontanni smrti, zabijime bunku
			if (probability < spontanious_death && !cells[o].stem) {
				updateMatrixAfterDeath(matrix, cells[o].position);
				continue;
			}

			if (probability < proliferate) {
				//proliferace jen tehdy, pokud je v okoli misto
				if (matrix[cells[o].position.y][cells[o].position.x] != 0) {
					//bunka se rozmnozi pouze, jestli je nazivu
					if ((!cells[o].stem && cells[o].health > 1) || (cells[o].stem)) {
						float stem_probability = dis(gen);

						unsigned char health = (cells[o].stem ? cells[o].health : --cells[o].health); //snizime zivotnost pouze u nekmenove bunky
						cell src_cell = { cells[o].position, cells[o].stem, health };

						bool stem = false;
						//pravdepodobnost asymetrickeho deleni jen u kmenovych bunek
						if (stem_probability < stem_birth && cells[o].stem) {
							stem = true;
						}

						//nova bunka
						cell new_cell = { pickFreePlace(matrix, cells[o].position.x, cells[o].position.y), stem, health };

						//vlozime do vektoru obe bunky
						tmp.push_back(src_cell);
						tmp.push_back(new_cell);

						//upravime matici
						updateMatrixAfterProliferation(matrix, new_cell.position);

						//nastavi toExpand na true, kdyz je potreba zvetsit matici a toExpand jeste neni nastaveno, jinak nedela nic
						if (!toExpand)
							toExpand = checkBoundary(matrix.size(), new_cell.position);
					} else {
						//bunka umira
						updateMatrixAfterDeath(matrix, cells[o].position);
					}
				} else {
					//okolo bunky neni misto
					tmp.push_back(cells[o]);
				}
			} else if (probability < proliferate + migration) {
				//posunuti jen tehdy, pokud je v okoli misto
				if (matrix[cells[o].position.y][cells[o].position.x] != 0) {
					//bunka se presunuje pouze jestli je nazivu
					if ((!cells[o].stem && cells[o].health > 1) || (cells[o].stem)) {
						cell new_cell = { pickFreePlace(matrix, cells[o].position.x, cells[o].position.y), cells[o].stem, cells[o].health };

						tmp.push_back(new_cell);

						//upravime matici
						updateMatrixAfterMove(matrix, cells[o].position, new_cell.position);

						//nastavi toExpand na true, kdyz je potreba zvetsit matici a toExpand jeste neni nastaveno, jinak nedela nic
						if (!toExpand)
							toExpand = checkBoundary(matrix.size(), new_cell.position);
					} else {
						//bunka umira
						updateMatrixAfterDeath(matrix, cells[o].position);
					}
				} else {
					//okolo bunky neni misto
					tmp.push_back(cells[o]);
				}
			} else {
				//bunka nedela nic (ani proliferace, ani presun)
				tmp.push_back(cells[o]);
			}
		}

		//vektor cells prepiseme docasnym vektorem tmp (aktualizace bunek)
		cells = tmp;

		//do souboru tiskneme po dni simulace (po 24 hodinach)
		if (i % 24 == 0) {
			high_resolution_clock::time_point t2 = high_resolution_clock::now();
			auto duration = duration_cast<milliseconds>(t2 - t1).count();
			int ms = duration % 1000; //milisekundy
			int s = (duration / 1000) % 60; //sekundy
			int m = (duration / 60000) % 60; //minuty
			int h = (duration / 3600000) % 100; //hodiny (zobrazi se az do 99)

			char str[12];
			sprintf(str, "%02d:%02d:%02d:%03d", h, m, s, ms);

			cout << "Cas: " << str << ", den " << image_index << ": pocet bunek: " << cells.size() << ", velikost matice: " << matrix.size() << endl;
			printCells(cells, image_index, matrix.size());
		}

		if (toExpand) {
			//rozsirime matici kdyz je to treba
			expandMatrix(matrix, cells, 10);
		}
	}
}

//hlavni funkce
int main(int argc, char const *argv[]) {
	int iterations = 1; //pocet dni simulace (vynasobi se 24, simulace probiha v hodinach)

	//zpracovani argumentu
	if (argc > 1) {
		iterations = atoi(argv[1]);
	} else {
		cerr << "Spusteni: ./ca <pocet dni simulace>" << endl;
	}

	vector<vector<unsigned char>> matrix; //matice, kam se ukladaji volna mista bunek
	vector<cell> cells; //vektor bunek
	initMatrix(matrix, 9); //inicializace matice
	initCells(matrix, cells); //inicializace bunek

	simulate(matrix, cells, iterations * 24); //samotny cyklus simulace

	return 0;
}
