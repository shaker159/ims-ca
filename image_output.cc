#include "image_output.h"

//vytvori modry obrazek o zadanych rozmerech
void initImage(vector<vector<color>> &image, int w, int h) {
	for (int i = 0; i < w; i++) {
		vector<color> tmp;
		for (int o = 0; o < h; o++) {
			color c = BLUE;
			tmp.push_back(c);
		}
		image.push_back(tmp);
	}
}

vector<vector<color>> resizeMatrix(vector<vector<color>> plocha, int size) {
	vector<vector<color>> plocha_resize;

	for (int i = 0; i < plocha.size(); ++i) {
		vector<color> tmp;
		for (int o = 0; o < plocha[i].size(); ++o){
			for (int s = 0; s < size; ++s){
				tmp.push_back(plocha[i][o]);
			}
		}
		for (int s = 0; s < size; ++s){
			plocha_resize.push_back(tmp);
		}
	}
	return plocha_resize;
}


void printMatrix(vector<vector<unsigned char>> plocha, int &image_index) {
	vector<vector<color>> image_matrix;
	color clr = BLACK;
	for (int i = 0; i < plocha.size(); ++i) {
		vector<color> tmp;
		for (int o = 0; o < plocha[i].size(); ++o) {
			if (plocha[i][o] == 9) {
				clr = WHITE;
				tmp.push_back(clr);
			} else {
				clr = BLACK;
				tmp.push_back(clr);
			}
		}
		image_matrix.push_back(tmp);
	}

	vector<vector<color>> expanded = image_matrix;
	// vector<vector<color>> expanded = resizeMatrix(image_matrix, 10);
	writeImage(expanded, "day" + to_string(image_index));
	image_index++;
}

void printCells(vector<cell> cells, int &image_index, int matrix_size) {
	vector<vector<color>> image_matrix;

	color colors[] = { RED1, RED2, RED3, RED4, RED5, RED6, RED7, RED8, RED9, RED10 };

	for (int i = 0; i < matrix_size; ++i) {
		vector<color> tmp;
		tmp.resize(matrix_size, WHITE);
		image_matrix.push_back(tmp);
	}

	for (int i = 0; i < cells.size(); ++i) {
		color c;

		if (cells[i].stem) {
			c = YELLOW;
		} else {
			c = colors[cells[i].health - 1];
		}

		image_matrix[cells[i].position.y][cells[i].position.x] = c;
	}

	vector<vector<color>> expanded = image_matrix;
	// vector<vector<color>> expanded = resizeMatrix(image_matrix, 10);
	writeImage(expanded, "day" + to_string(image_index));
	image_index++;
}

//zapise obrazek do souboru
int writeImage(vector<vector<color>> &image, string filename) {
	ofstream f;
	f.open(filename);
	f << "P3\n";
	f << image[0].size() << " " << image.size() << "\n";
	f << "255\n";

	for (int i = 0; i < image.size(); i++) {
		for (int o = 0; o < image[i].size(); o++) {
			f << image[i][o].r << " ";
			f << image[i][o].g << " ";
			f << image[i][o].b;

			if (o != image[i].size() - 1) {
				f << " ";
			}
		}
		f << "\n";
	}

	f.close();

	return 0;
}
