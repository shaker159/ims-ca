#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include "image_output.h"

#define NOINIT -10

using namespace std;

//struktura bunky, obsahuje val, coz je stav bunky a neighbourhood, coz je bitovy vektor okoli
struct bunka {
	int ttl;
	int x;
	int y;
	bool stem;
	bool vacant;
	color his_color;
};

struct place_of_hit
{
	int x;
	int y;
};
place_of_hit neighbourhood[8] = {{-1,-1},{0,-1},{1,-1},{-1,0},{1,0},{-1,1},{0,1},{1,1}};
/*
* Nainicializuje matici na size*size
*/
void init(vector<vector<unsigned char>> &plocha, int size) {
	for (int i = 0; i < size; ++i) {
		vector<unsigned char> sloupec;
		for (int o = 0; o < size; ++o) {
			sloupec.push_back(0);
		}
		plocha.push_back(sloupec);
	}
}
void initCells(vector<vector<unsigned char>> &plocha,vector<bunka> &cells){
	bunka new_cell = {-1,5,5,true,true};

	cells.push_back(new_cell);
	plocha[new_cell.x][new_cell.y] = 1;

}
void removeCellshistory(vector<vector<unsigned char>> &plocha){ // funkce ktera maze aktualni historii
	for (int i = 0; i < plocha.size(); ++i) {
		for (int o = 0; o < plocha[i].size(); ++o) {
			if(plocha[i][o] == 3){
				plocha[i][o] = 0;
			}
		}
	}
}



place_of_hit discoverNeighbourhood(vector<vector<unsigned char>> &plocha, int x, int y) {
//  0 1 2
//  3 x 4
//  4 5 6	
	random_shuffle(std::begin(neighbourhood), std::end(neighbourhood));
	//NOINIT je -10, znaci to ze nebylo nic nalezeno.
	//ovsem neco takoveho by se u teto funkce nemelo stat, tato funkce se vola jen pro ty co maji kolem sebe volno
	place_of_hit succes_hit = {NOINIT,NOINIT};  
	for (unsigned char i = 0; i < 8; ++i){
		// cout << "x:"<< 5 + neighbourhood[i].x <<", y:"<< 5 + neighbourhood[i].y << endl;
		if (plocha[y + neighbourhood[i].y][x + neighbourhood[i].x] == 0) {
			succes_hit.x = x + neighbourhood[i].x;
			succes_hit.y = y + neighbourhood[i].y;
			return succes_hit;
		}
		
	}
	return succes_hit;
}
/*
* funkce pouzivana funkci updateMatrix
* zjisti zda je kolem bunky volno
* pokud najde prvni volne hned vraci true 
*/
bool isVacant(vector<vector<unsigned char>> plocha, int x, int y){ // funkce pouzivana funkci updateMatrix
	for (int i = -1; i < 2; ++i) {
		for (int o = -1; o < 2; ++o) {
			//prochazime jen okoli bunky (kdyz je i a o == 0, tak jsme na pozici bunky)
			if (i != 0 && o != 0) {
				if ((plocha[y + o][x + i] == 0) || (plocha[y + o][x + i] == 3)){ // porc 3? kvuli validaci , lepe se nam bude overovat platnost pokud uvidime historii, kde bunky byli
					return 1;
				}
			} else
				continue; //kdyz jsme na pozici bunky, nic nedelame a pokracujeme dal
		}
	}
	return 0;
}
/*
* funkce pro update infromace zda ma bunka volneho souseda/sousedy
* update je nutny po procesu, kdy bunky migruji nebo proliferuji, potom je treba u vsech aktualizovat, jestli maji volno nebo jsou zaplnene
* muze se stat, ze bunka, ktera ma volne misto bude na konci po celkovem procesu (jedna epocha zmeny cele matice) vsude zaplnena
*/
void updateMatrix(vector<bunka> &cells, vector<vector<unsigned char>> &plocha){
	for(int i=0; i < cells.size();i++){
		if(isVacant(plocha,cells[i].x,cells[i].y)){ // pokud je kolem dane bunky prazdno tak ji nastavuji jako vacant
			cells[i].vacant = 1;
		}else{
			cells[i].vacant = 0;
		}
	}
}
void proces(vector<bunka> &cells, vector<vector<unsigned char>> &plocha){
	int random_index = 0;
	bunka choosen_cell;
	bunka new_cell = {-1,5,5,true,true};
	int action_propability;
	vector<bunka> processed;
	place_of_hit ran_vacant_place;
	unsigned char index_of_place;

	while (!cells.empty())
  	{
		random_index = 0 + (rand() % (int)(cells.size() - 0));
		choosen_cell = cells[random_index];
		cells.erase(cells.begin()+random_index); // odstranim zpracovanou bunku

		if(choosen_cell.vacant == 0){ // pokud je bunka zcela obklopena sousedy nic s ni nedelam;
			processed.push_back(choosen_cell);
			continue;
		}
		action_propability = 0 + (rand() % (int)(100 - 0 + 1)); // pravdepodobnost s jakou bunka migruje nebo proliferuje
		ran_vacant_place = discoverNeighbourhood(plocha,choosen_cell.x, choosen_cell.y); // tato funkce vraci index do pole neighbourhood
		cout << action_propability << endl;
		if(action_propability > 50){ // tady proliferuje	
			new_cell.x = ran_vacant_place.x; //xova souradnice pro novou bunku, pravou adresu vypocitam uz v discoverNeighbourhood
			new_cell.y = ran_vacant_place.y; //yova souradnice pro novou bunku, pravou adresu vypocitam uz v discoverNeighbourhood
			plocha[new_cell.y][new_cell.x] = 1;
			processed.push_back(choosen_cell);
			processed.push_back(new_cell);
		}else{
			new_cell.x = ran_vacant_place.x; //xova souradnice pro novou bunku
			new_cell.y = ran_vacant_place.y; //yova souradnice pro novou bunku
			plocha[new_cell.y][new_cell.x] = 2; //2 pro blue bude oznacovat bunku ktera migrovala
			plocha[choosen_cell.y][choosen_cell.x] = 3; // cili 3 pro black bude historie, pro lesi overitelnost bude vmatici i histore bunky
			processed.push_back(new_cell);
		}
  	}
  	cells = processed; // nakonec 
   	updateMatrix(cells,plocha);
   	
}

/**
* Vypise vsechny bunky
*/
vector<vector<unsigned char>> resizeMatrix(vector<vector<unsigned char>> plocha,int size){
	vector<vector<unsigned char>> plocha_resize;

	for (int i = 0; i < plocha.size(); ++i) {
		vector<unsigned char> tmp;
		for (int o = 0; o < plocha[i].size(); ++o){
			for (int s = 0; s < size; ++s){
				tmp.push_back(plocha[i][o]);
			}
		}
		for (int s = 0; s < size; ++s){
			plocha_resize.push_back(tmp);
		}
	}
	return plocha_resize;
}


void printAll(vector<vector<unsigned char>> plocha, int &image_index) {
	vector<vector<color>> image_matrix;
	vector<vector<unsigned char>> plocha_resize = resizeMatrix(plocha, 10);
	color clr = BLACK;
	for (int i = 0; i < plocha_resize.size(); ++i) {
		vector<color> tmp;
		for (int o = 0; o < plocha_resize[i].size(); ++o) {
			// cout  << +plocha_resize[i][o] << "";

			switch(plocha_resize[i][o]){
				case 0:
					clr = WHITE;
				 	tmp.push_back(clr);
				 	break;
				case 1:
					clr = GREEN;
					tmp.push_back(clr);
				 	break;
				 case 2:
				 	clr = BLUE;
					tmp.push_back(clr);
				 	break;
				 case 3:
				 	clr = BLACK;
					tmp.push_back(clr);
				 	break;

			}
		}
		image_matrix.push_back(tmp);
		// cout << endl;
	}

	int jechyba = writeImage(image_matrix, "epocha"+to_string(image_index));
	cout << endl << jechyba << endl;
	image_index++;
}

void printSimple(vector<vector<unsigned char>> plocha) {
	for (int i = 0; i < plocha.size(); ++i) {
		for (int o = 0; o < plocha[i].size(); ++o) {
			cout  << +plocha[i][o] << "";
		}
		cout << endl;
	}
}

/*
* rozsiri matici do vsech stran o velikost size
*/
void expandMatrix(vector<vector<unsigned char>> &plocha, vector<bunka> &cells, int size) {
	int newsize = plocha.size() + 2 * size;
	vector<vector<unsigned char>> v;
	v.resize(newsize);

	for (int i = 0; i < v.size(); ++i) {
		v[i].resize(newsize);
	}

	for (int i = 0; i < plocha.size(); ++i) {
		for (int o = 0; o < plocha[i].size(); ++o) {
			v[i + size][o + size] = plocha[i][o];
		}
	}

	plocha = v;

	for (int i = 0; i < cells.size(); ++i) {
		cells[i].x += size;
		cells[i].y += size;
	}
}

/**
* Hlavni funkce
*/
int main(int argc, char const *argv[]) {
	srand(time(0));
	vector<vector<unsigned char>> plocha;
	vector<bunka> cells;
	int image_index = 0;
	init(plocha, 10);
	initCells(plocha, cells);

	for( int i = 0; i < 6; i++){ // necham udelat deset promen, jako test co to dela
	// cout << "tady vse jde";
		proces(cells, plocha);
		printAll(plocha, image_index);
		// printSimple(plocha);
		removeCellshistory(plocha);
		cout << endl;

		expandMatrix(plocha, cells, 10);
	}
	// proces(cells);
	// printAll(plocha);

	return 0;
}
