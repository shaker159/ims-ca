#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "image_output.h"

using namespace std;

struct file {
	int x;
	int y;
	bool is_stem;
	int p;
	int nFreeSpots;
};

int main(int argc, char const *argv[])
{
	if (argc != 2)
		return 1;

	vector<cell> contents;

	ifstream f;
	f.open(argv[1]);

	string str;

	int i = 0;
	bool firstline = true;

	int max = 0;

	if (f.is_open()) {
		while (!f.eof()) {
			if (firstline) {
				getline(f, str);
				firstline = false;
			} else {
				// cout << i << endl;
				getline(f, str);

				string item;
				int item_cnt = 0;
				cell content;

				for (string::const_iterator it = str.begin(); it != str.end(); ++it) {
					if (*it == ',' || *it == ' ') {
						if (item != "") {
							if (item_cnt == 0) {
								content.position.x = stoi(item);
							} else if (item_cnt == 1) {
								content.position.y = stoi(item);
							} else if (item_cnt == 2) {
								content.stem = (stoi(item) == 1);
							} else if (item_cnt == 3) {
								content.health = (unsigned char)stoi(item);
							}
							// cout << item << endl;

							item.clear();

							++item_cnt;
						}
					} else {
						item += *it;
					}
				}

				if (content.position.x > max)
					max = content.position.x;

				if (content.position.y > max)
					max = content.position.y;

				// cout << content.position.x << content.position.y << endl;
				contents.push_back(content);

				// cout << str << endl;
				++i;
			}
		}

		int image_index = 0;
		printCells(contents, image_index, max + 1);
	}
	f.close();

	return 0;
}