all: ca 

ca: ca.cc
	g++ -std=c++11 ca.cc image_output.cc -o ca

debug: ca.cc
	g++ -std=c++11 -g ca.cc image_output.cc -o debug

pack: 
	zip 04_xmorav28_xcervi19 ca.cc Makefile image_output.cc image_output.h dokumentace.pdf

clean:
	-rm ca day*

run:
	./ca 180
